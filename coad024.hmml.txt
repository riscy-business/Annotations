0:01 Recap and set the stage for the day
1:41 Memory allocation in Linux, using brk
3:20 Chapter 2.8 continued, Allocating Space for New Data on the Heap: https://www.elsevier.com/books/computer-organization-and-design-risc-v-edition/patterson/978-0-12-812275-4
3:32 Static and automatic storage duration in C
7:03 Static storage
8:48 Static storage, as used in pcalc: https://gitlab.com/riscy-business/pcalc
13:06 Global variables, as used in pcalc
18:15 A general critique of global variables
21:19 Uninitialised vs initialised static and automatic storage duration variables in pcalc (uninitialised automatic storage duration variables are undefined)
32:05 The "auto" keyword in C
33:47 Chapter 2.8 continued, Allocating Space for New Data on the Heap
36:41 Figure 2.13 - The RISC-V memory allocation for program and data
44:24 The fixed nature of the stack
46:41 Tip of the day: Get into sponge mode
47:52 Figure 2.13 continued - The RISC-V memory allocation for program and data
52:09 Chapter 2.8 continued, on memory allocation
52:57 General-purpose memory allocation and garbage collection
57:48 Chat comment: "are you not interested in Ada programming as it is a more robust and reliable language? AdaCore now supports RISC-V!"
59:13 Continued thoughts on memory allocation and freeing, including swapping to disk and performance
1:06:01 Legitimate memory leaking strategies
1:10:59 Dangling pointers, prevented by zeroing out pointers
1:17:30 Garbage collection, as used by pcalc's build.d script
1:24:18 Chapter 2.8 continued
1:25:48 Chapter 2.8 Elaboration 2 - Making the common case, that procedures take eight or fewer parameters, fast
1:35:55 On needing a frame pointer when a procedure's memory requirements are not known at compile-time
1:37:26 Figure 2.14 - RISC-V register conventions: https://riscv.org/specifications
1:42:38 Chapter 2.8 Elaboration 3 - Implementing recursive procedures iteratively
1:45:41 Implementing a recursive accumulation procedure iteratively
2:03:54 The "--&gt;" operator (don't try this at home!)
2:07:08 Completing our recursive-to-iterative accumulation procedure conversion
2:12:17 Lower the recursive accumulation procedure to assembly: http://www.riscvbook.com/
2:21:24 We are out of time
2:22:02 Chat comment: "I think first I need to watch your previous streams in the sequence for a better understanding: https://riscy.handmade.network/episode/coad"
2:23:57 End this episode here

Annotated by Miblo - https://handmade.network/m/Miblo
