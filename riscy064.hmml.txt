0:21 Recap our RISC-V PC plan and set the stage for the day
4:35 Present the news of a RISC-V chip with built-in neural networks - https://hackaday.com/2018/10/08/new-part-day-the-risc-v-chip-with-built-in-neural-networks/2 - and a new video in Robert Baruch's LMARV-1 series: https://twitter.com/babbageboole/status/1049052861932457984
8:25 Plug Colin Riley's TPU/RPU Series designing a RISC-V CPU in VHDL: http://labs.domipheus.com/blog/designing-a-risc-v-cpu-in-vhdl-part-16-arty-s7-rpu-soc-block-rams-720p-hdmi/
8:50 Chat comment: "Kestrol?"
10:21 Continue to plug Colin's TPU/RPU series
10:50 Plug domas's project rosenbridge, revealing hardware back doors in CPUs: https://twitter.com/xoreaxeaxeax/status/1027642170860163072 and https://www.youtube.com/watch?v=_eSAF_qT_FY
19:52 Present the news of Bloomberg's fake "Big Hack" story - https://twitter.com/qrs/status/1047788385425940480 - and Patrick Gray's follow-up piece on the story: https://twitter.com/riskybusiness/status/1049429881031819264
20:42 Chat comment: "Fake news from the dying Bloomberg Times"
20:49 Continued thoughts on trusting our supply chain in response to the Bloomberg story - https://twitter.com/qrs/status/1047788385425940480 - including Patrick Gray's follow-up piece on the story: https://twitter.com/riskybusiness/status/1049429881031819264
22:44 Chat comment: "Still, strange that a reputable news agency was so assertive this was real. Maybe there's more to it than we know"
23:05 That's all for the news
24:06 Set up to follow Atish Patra's instructions on building Fedora for the HiFive Unleashed - https://github.com/westerndigitalcorporation/RISC-V-Linux - using our newly ordered hardware: https://twitter.com/hmn_riscy/status/1050314126705213440
29:31 Chat comment: "This message brought to you by Amazon..."
30:28 Getting the Freedom U SDK to build on an x86 laptop running Arch Linux: https://github.com/westerndigitalcorporation/RISC-V-Linux
39:30 Chat comment: "That's a lot of dependencies"
40:23 Continue to bring us up to speed with our progress through Atish Patra's instructions on building Fedora for the HiFive Unleashed
41:50 Unbox our 250GB Samsung 970 EVO NVMe M.2 SSD
47:24 Unbox our SuperSpeed USB 3.0 PCI-e Host Card
54:20 Consider our power requirements in light of the fact that the HiFive Unleashed powers the expansion board
57:22 Chat comment: "Like an IEC connector?"
57:45 Continue to consider our power requirements
59:42 Chat comment: "Later"
59:46 Continue to consider our power requirements, rigging up the power button
1:01:35 Consider our case design for modular mounting
1:16:54 Chat comment: "Are you trying to make it ATX-like?: https://www.sfflab.com/products/nfc_s4m"
1:20:38 Continue with our graphics card mounting considerations
1:26:13 Research case fabrication: https://www.emachineshop.com/custom-metal-fabrication/
1:30:03 Chat comment: "Sheet metal is flimsy, but it gets its structure / rigidity from its bends / right angles / supports"
1:30:22 Continue to read about metal fabrication
1:32:45 Wrap it up with a glimpse into the future of our RISC-V PC and case fabrication
1:33:49 Chat comment: "There is additive printing with metal, but that's pretty cutting edge... NASA actually started experimenting with 3D printing rocket valves..."
1:34:35 "Stay RISCY, everyone"

Annotated by Miblo - https://handmade.network/m/Miblo
