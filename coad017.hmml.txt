0:12 Shout-out to Wasim with a note on the stream organisation
2:55 Plug the new RISCY BUSINESS and Book Club Early Access site: https://guide.riscy.tv/
7:03 Thoughts on LTR vs RTL languages
9:25 Wasim's perspective on numbers in RTL languages
11:49 Set up to dive into Chapter 2.4, noting the two's complement adder we described on RISCY BUSINESS: https://riscy.handmade.network/episode/riscy#two's%20complement
17:43 Chapter 2.4 Signed and Unsigned Numbers: https://www.elsevier.com/books/computer-organization-and-design-risc-v-edition/patterson/978-0-12-812275-4
19:20 Information theory and the "bit" as the fundamental unit of data
20:57 Chapter 2.4 continued
21:25 Numerical base
29:30 Plug pcalc, in-development but already available to backers at $5 or more
30:35 Understanding bases of unsigned numbers
32:51 Chapter 2.4 continued
37:33 Bytes split into nibbles
44:25 Why 64-bits? Because that's the register width of the HiFive Unleashed
49:06 Chapter 2.4 continued, including bit significance and the RISC-V doubleword bit length
52:28 Understanding sums of powers
57:11 Chapter 2.4 continued, 64-bit unsigned number representations
57:53 Tip of the day: Counting binary numbers on your hand
1:03:21 Chapter 2.4 continued, 64-bit unsigned number representations
1:05:20 Representing negative numbers
1:07:10 Chapter 2.4 continued, 64-bit unsigned number representations
1:07:53 Chapter 2.4 Hardware / Software Interface
1:08:08 "Natural" bases in human history
1:11:31 Chapter 2.4 Hardware / Software Interface, continued
1:11:39 Tidbit: Various bases in computer history
1:13:02 Chapter 2.4 Hardware / Software Interface, continued
1:14:19 On the physical constraints of computers in terms of their numerical representation
1:18:54 Chapter 2.4 continued, on overflow
1:19:21 Thoughts on overflow / underflow, and programming for machines
1:22:00 Chapter 2.4 continued, on sign and magnitude
1:22:26 Two's complement as a way to represent both positive and negative numbers on the same hardware: https://riscy.handmade.network/episode/riscy#two's%20complement
1:23:58 Chapter 2.4 continued, shortcomings of sign and magnitude
1:25:23 Positive and negative 0
1:28:14 Chapter 2.4 continued, two's complement
1:34:10 Understanding negative numbers in two's complement, by counting with 0s
1:39:00 Chapter 2.4 continued, positive and negative numbers in two's complement
1:43:54 On two's complement being good for hardware designers: https://riscy.handmade.network/episode/riscy/riscy012/#596
1:46:56 Chapter 2.4 continued, two's complement
1:47:30 Efficient sign-checking in two's complement
1:50:11 Chapter 2.4 continued, two's complement
1:52:35 Chapter 2.4 Example 1 - Binary to Decimal Conversion
1:59:30 End it here with a glimpse into the next episode, thoughts on it all tying in to RISCY BUSINESS and thanks for all the support

Annotated by Miblo - https://handmade.network/m/Miblo
