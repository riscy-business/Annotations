0:07 Set the stage for reading section 1.4
0:34 Chapter 1.4 - Under the Covers: https://www.elsevier.com/books/computer-organization-and-design-risc-v-edition/patterson/978-0-12-812275-4
3:09 Figure 1.5 - The standard organisation of a computer
9:33 Through the looking glass
9:53 A few words on the underappreciated importance of graphical displays
11:44 Through the looking glass (cont.)
14:19 A few words on 's own monitor setup
15:31 Through the looking glass (cont.)
16:06 Figure 1.6 - Framebuffer
19:20 Touch screen
20:28 Opening the box
24:02 Chat comment: "What is this book?"
24:54 A few words on latencies being magnified when making RAM in Minecraft
25:50 Opening the box (cont.)
32:12 Figure 1.8 - The logic board of Apple iPad 2
34:06 A suggestion to buy lower spec systems with a view to upgrading them
35:29 Figure 1.9 - Processor Integrated Circuit
40:04 Cache memory
42:13 A few words on the amount of time we spend studying instruction set documentation in riscy
45:28 Hunt for a reference in the hero site
47:07 Plug the ability of insobot to pull stream schedules from twitter: https://abaines.me.uk/insobot/schedule
48:49 Compare the size of the Intel 64 and IA-32 Architectures Software Developer's Manual - https://software.intel.com/sites/default/files/managed/39/c5/325462-sdm-vol-1-2abcd-3abcd.pdf - with the RISC-V architecture
54:24 Instruction set architecture
55:48 The hierarchy of HiFive1 documentation: E3 Coreplex - https://static.dev.sifive.com/pdfjs/web/viewer.html?file=https://static.dev.sifive.com/SiFive-E3-Coreplex-v1.2.pdf - → E300 - https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/ - → FE310-G000: https://www.sifive.com/documentation/chips/freedom-e310-g000-manual/
58:15 A safe place for data
1:00:51 Note that SSD is flash-based storage and does wear out
1:02:04 Communicating with other computers
1:05:03 A few words on physically mailing magnetic tapes and the ping time between Mars and Earth that Elon Musk may need to consider in his endeavours to establish a colony on Mars
1:07:05 Communicating with other computers (cont.)
1:09:43 Check yourself: Compare the volatility, access time and cost of semiconductor, DRAM and flash memory and disk storage
1:10:46 Spare us the "check yourself" and reflect on chapter 1.4
1:12:30 That will be it for tonight

Annotated by Miblo - https://handmade.network/m/Miblo
