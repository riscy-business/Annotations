0:07 Recap and set the stage for the day
0:45 Read about E300 Power, Reset, Clock, Interrupt (PRCI) Control and Status Registers: https://static.dev.sifive.com/pdfjs/web/viewer.html?file=https://static.dev.sifive.com/SiFive-E3-Coreplex-v1.2.pdf
2:57 Marry up the PRCI_REG macro and the PRCI register offsets from the code with the documentation
5:22 Hunt for further documentation on the PRCI
9:01 Read about E300 Clock Generation: https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/
14:04 A few thoughts on ring oscillator and trim
15:36 Chat comment: "Just a chain of not gates"
15:40 Continued thoughts on how the trim value works
16:35 Continue reading about the Internal Trimmable Programmable 72 MHz Oscillator (HFROSC)
20:29 Read about External 16 MHz Crystal Oscilllator (HFXOSC)
22:35 Chat comment: "ESR: equivalent series resistance"
22:47 Continue reading about the HFXOSC
23:31 Chat comment: "Voltage-controlled oscillator"
23:40 Continue reading about HFXOSC
24:47 Read about Internal High-Frequency PLL (HFPLL) and Phase-locked loop: https://en.wikipedia.org/wiki/Phase-locked_loop
27:59 Determine to add to the plan
30:17 Chat comment: "i.e. an assembler ;)"
30:31 Add "compare a demo's asm to expected", "study the hardware implementation" and "write software" to the plan
31:52 Continue reading about HFPLL: https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/
35:18 A few thoughts on how the PLL relates to the marketed clock rate of the HiFive1
37:04 Chat comment: "The PPL generates the higher clock rates from the crystal"
37:59 Chat comment: "Probably not within spec"
38:25 Consider overclocking a HiFive1 (not our main one, though!)
39:20 Continue reading about HFPLL
42:23 Chat comment: "When they say 0 is not supported I'm imagining it's because the core doesn't run that quickly, not because that divider wouldn't work"
43:18 Continue reading about HFPLL
49:49 Read about PLL
49:54 Chat comment: "VDD and VSS are power pins"
50:00 Read about PLL Output Divider
51:15 Read about Internal Low-Frequency Oscillator (LFRCOSC)
51:55 Read about External 32.768 kHz Low-Frequency Crystal Oscillator (LFXOSC)
52:39 Determine that we fully understand the opening code of led_fade.c and consult the PLL_* macros
55:19 Cross off "PRCI" and move on to "Other Demos"
56:34 Embark on studying performance_counters.c
1:00:34 Read 3.1.15 Hardware Performance Monitor: https://riscv.org/specifications/privileged-isa/
1:03:34 Continue reading the rdmcycle() macro
1:04:29 Read about BNE in 2.5 Control Transfer Instructions and CSRRS in 2.8 Control and Status Register Instructions: https://riscv.org/specifications
1:09:50 Summarise how rdmcycle() is checking for rollover into the high half of the register
1:10:53 Chat comment: "Not rollover completely, only increment in the part of mcycleh"
1:11:32 Wonder if the 1b in rdmcycle() means 1 in binary
1:12:26 We are out of time for today
1:12:39 Chat comment: "Skip over what?"
1:13:39 Determine to investigate deeper in the next episode

Annotated by Miblo - https://handmade.network/m/Miblo
