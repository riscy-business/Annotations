0:01 Recap and set the stage for the day
1:49 Review where we're at
2:27 Chapter 2.4 - RV32I Integer Computation: http://www.riscvbook.com/
3:43 A few words on the Spectre and Meltdown vulnerabilities due to branch-prediction and speculative execution
8:19 Chapter 2.4 continued
10:08 Point out the mistake in the book's description of the "call" pseudo-instruction
13:30 Chapter 2.4 continued, on RISC-V's simple arithmetic instructions
16:47 A few words of praise for the concision of the RISC-V base ISA
19:36 Plug Casey's Meow hash - https://twitter.com/cmuratori/status/1053453319299977221 - Mārtiņš's ports to ARMv8 - https://twitter.com/mmozeiko/status/1054786688361279490 - and C without special hardware instructions - https://twitter.com/mmozeiko/status/1054942982099496960 - and Miblo's plea for a RISC-V port - https://twitter.com/miblo_/status/1054800216371707904 - which will happily contribute - https://twitter.com/miotatsu/status/1054894248603148293 - once RISC-V gets the proposed vector and crypto extensions: https://content.riscv.org/wp-content/uploads/2017/12/Wed-1418-RISCV-RichardNewell.pdf
37:01 Chapter 2.4 continued, on RISC-V's comparison and branching instructions
39:15 Summarise the concept of auipc and jal to allow for 32-bit immediates
46:16 Figure 2.4 - The registers of RV32I: https://riscv.org/specifications
52:27 Chapter 2.4 continued, on the differences between RISC-V and ARM
55:36 Chapter 2.4 Elaboration 1 - "Bit twiddling" instructions
55:54 Chapter 2.4 Elaboration 2 - xor enables a magic trick
59:47 XOR swap, thanks to algebraic reversibility
1:19:12 Chapter 2.4 Elaboration 2 continued
1:21:54 XOR linked list, again thanks to algebraic reversibility: https://en.wikipedia.org/wiki/XOR_swap_algorithm and https://en.wikipedia.org/wiki/XOR_linked_list
1:43:36 Chapter 2.5 - RV32I Loads and Stores
1:46:23 Recommend Fabian's videos on CPU µArch: https://www.youtube.com/channel/UCcRaa0AcYX32c0m8wJJHNWg/videos
1:50:55 Chapter 2.5 continued, on differences in load / store instruction between RISC-V and MIPS and ARM
1:51:52 Recommend Robert Baruch's LMARV-1 video series: https://www.youtube.com/playlist?list=PLEeZWGE3PwbansoxKjjMKHQqS_2cm8i60
1:53:23 Chapter 2.5 Elaboration - Endianness
1:55:27 Endianness from Gulliver's Travels: https://en.wikipedia.org/wiki/Gulliver%27s_Travels
1:59:19 That's the end of 2.5
1:59:59 Chat comment: "I missed this episode"
2:02:06 Endianness in practice in pcalc: https://gitlab.com/riscy-business/pcalc
2:02:18 Chat comment: "I have a question for you: Is hamming distance implemented in RISC-V ISA and the compiler for more energy / code efficiency?"
2:05:31 Endianness in practice in pcalc continued, including connecting to an X11 server using the ~/.Xauthority file
2:12:05 End the episode there

Annotated by Miblo - https://handmade.network/m/Miblo
