0:07 Recap and set the stage for the day, with a plug of the RISCY BUSINESS GitHub: https://github.com/riscy-business
6:08 Embark on studying entry.S, our first substantial look at GNU assembly
11:05 Adding to the stack pointer
14:39 mcause and mepc to the csrr
17:11 Describe stack and heap memory, including brk: https://linux.die.net/man/2/brk
22:15 Hunt the documentation for the register names: https://riscv.org/specifications
27:18 Plug SiFive's launch announcement of the U54-MC Coreplex - https://www.sifive.com/posts/2017/10/04/sifive-launches-first-risc-v-based-cpu-core-with-linux-support/ - and Raptor Computing Systems - https://www.raptorcs.com/ - with dreams for a RISC-V desktop
36:28 Continue the description of stack and heap memory allocation
39:04 Plug nwr_mem.h, a user-space memory allocator: https://riscy.handmade.network/forums/t/2471-nwr_mem.h
42:30 Argument registers: a0, a1 and a2: https://github.com/riscv/riscv-elf-psabi-doc/blob/master/riscv-elf.md
47:12 Hunt the documentation for the control status register: https://riscv.org/specifications/privileged-isa/
49:46 Read about the Machine Exception Program Counter (mepc) and Machine Cause Register (mcause)
56:52 Refresh our memories about the CSR Instructions: https://riscv.org/specifications
58:58 Reading the exception data and stack pointer into argument registers: https://github.com/riscv/riscv-elf-psabi-doc/blob/master/riscv-elf.md
1:02:19 Calling handle_trap and writing the return value into mepc: https://riscv.org/specifications
1:05:17 Read about the Machine Exception Program Counter (mepc): https://riscv.org/specifications/privileged-isa/
1:06:28 Study handle_trap() in init.c, wondering why write the stack pointer into a2
1:09:54 Draft questions in the Big RISCY BUSINESS Question Thread as to why entry.S writes the stack pointer into the trap handler, and the trap handler's return value into mepc: https://forums.sifive.com/t/the-big-riscy-business-question-thread/531/4
1:14:54 Continue reading entry.S, using li (load immediate) to load MSTATUS_MPP into a temporary register, and then storing that in mstatus
1:21:48 Read about the Machine Status Register (mstatus) - https://riscv.org/specifications/privileged-isa/ - and CSRS: https://riscv.org/specifications
1:25:22 Consult encoding.h for the definition of MSTATUS_MPP, and locate this in the mstatus register: https://riscv.org/specifications/privileged-isa/
1:30:04 Read about Memory Privilege in mstatus Register
1:32:37 Plug the SiFive webinar: https://info.sifive.com/risc-v-webinar
1:37:02 Read about MPP and MRET: https://riscv.org/specifications/privileged-isa/
1:44:19 Wonder if we swap MPP with the privileged mode (thus switching to user-mode), or just store the privileged mode into MPP
1:45:30 Continue studying entry.S: staying in machine-mode, and then restoring our registers and deallocate
1:50:59 Read about the Machine-Mode Trap-Return Instruction (MRET)
1:55:13 Determine to learn the GNU assembler syntax: https://sourceware.org/binutils/docs-2.28/as/index.html
2:00:28 Wrap it up with thoughts about sleep and streaming schedules
2:03:52 Shout-out to DannyFritz for the support

Annotated by Miblo - https://handmade.network/m/Miblo
