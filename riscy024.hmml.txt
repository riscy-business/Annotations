0:07 Plug SiFive's shout-out of the interview with Andrew and Yunsup: https://twitter.com/SiFiveInc/status/849698515915685888
0:22 Consult the forums for Megan's advice on finding the OTP: https://forums.sifive.com/t/lots-of-questions-riscy-business-day-21/434
1:31 Consult the forums for Megan's advice on enabling the HiFive1 to reliably start our programs: https://forums.sifive.com/t/does-anyone-else-have-problems-with-output/446
4:29 Open our files
8:42 Enable uart_init() to perform a busy loop
13:39 Run our program to see similarly sketchy outputting behaviour
14:08 Enable uart_init() to loop over INT_MAX: http://en.cppreference.com/w/c/types/limits
17:18 Run our program, see no blinking lights and wonder why
20:31 See the lights, the let the program run for a little bit
21:28 Try looping over INT_MAX on the dev machine
22:21 Run our test and estimate that it took five seconds
22:56 See the properly printed output of print_instructions()
23:05 Try to make uart_init() loop over a smaller number
24:14 Run our program and see that that worked perfectly
24:57 Enable print_instructions() to print out the data at 0x21FE4: https://forums.sifive.com/t/lots-of-questions-riscy-business-day-21/434
26:00 Chat comment: "0x7FFF. I thought it would be 0xFFFF"
27:01 Self-plug the episode on ripple carry adders: https://www.youtube.com/watch?v=BObABjzvVPw
27:50 Continue with print_instructions()
28:22 Run our program and see a potentially valid Unix timestamp
29:18 Note that the OTP content locations are offsets: https://static.dev.sifive.com/dev-kits/hifive1/hifive1-getting-started-v1.0.2.pdf
30:32 Consult the Platform Reference Manual on the E300 Platform Memory Map: https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/
31:30 Consult the E3 Coreplex Memory Map table for the address of the OTP read port: https://static.dev.sifive.com/pdfjs/web/viewer.html?file=https://static.dev.sifive.com/SiFive-E3-Coreplex-v1.2.pdf
32:49 Chat comment: "Well, it appears to be 0x7FFFFFFF or 1 &lt;&lt; 31 - 1"
33:25 Recap the intuition on two's complement, that you count with zeros
34:42 Report the value at 0x21FE4 in the forums: https://forums.sifive.com/t/lots-of-questions-riscy-business-day-21/434
37:06 Request for viewers' board ID values at 0x21FE4
38:11 Chat comment: "Yeah, I don't have the SDK installed yet, so I'll have to report back"
38:32 Scan quickly through the manuals looking for GPIO documentation
41:15 Read about General Purpose Input/Output Controller (GPIO) in the Platform Reference Manual: https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/
47:02 Read about level-triggered and edge-trigger interrupts on Wikipedia - https://en.wikipedia.org/wiki/Interrupt - in conjunction with Megan's post about how the UART works: https://forums.sifive.com/t/does-anyone-else-have-problems-with-output/446
51:11 Continue reading about the GPIO's Interrupts in the Platform Reference Manual: https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/
54:41 Research GPIO pull-up: http://raspberrypi.stackexchange.com/questions/4569/what-is-a-pull-up-resistor-what-does-it-do-and-why-is-it-needed
57:05 Continue reading about Internal Pull-Ups and the remaining information about Interrupts in the Platform Reference Manual: https://www.sifive.com/documentation/freedom-soc/freedom-e300-platform-reference-manual/
1:00:27 We are out time for today

Annotated by Miblo - https://handmade.network/m/Miblo
