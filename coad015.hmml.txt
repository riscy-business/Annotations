0:13 Recap and set the stage for the day, with thoughts on comparing the code sizes of various languages
2:58 Perform a more rigorous "Hello, World!" comparison between C and x86 asm
9:57 Chat comment: "Just passing by seeing what’s on and I heard I don’t need sake"
10:34 Note that start.S will be shared between asm and C
11:02 Chat comment: "Now I’m clueless"
11:38 Chat comment: "I just found you"
12:13 Pull up the existing clang invocation for test.c
13:01 Chat comment: "I’m down to learn"
14:24 Build and consult the objdump for our program
15:54 Create hello.S
17:02 Chat comment: "I must absorb your knowledge so I can become more powerful"
17:42 Remove excess lines from hello.S
19:15 Compare the lines of code in hello.S and test.c
21:50 Chapter 2.3 - Operands of the Computer Hardware: https://www.elsevier.com/books/computer-organization-and-design-risc-v-edition/patterson/978-0-12-812275-4
24:41 Sketch out a way to deal with the limited number of hardware registers, with thoughts on portable software
41:12 Chapter 2.3 continued
44:10 Recall Casey's explanation of how the speed of light can become a bottleneck: https://www.youtube.com/watch?v=T4CjOB0y9nI
52:49 Chapter 2.3 continued
55:40 Chapter 2.3 Example 1 - Compiling a C Assignment Using Registers
56:29 Our answer to Chapter 2.1 Example 2
1:00:32 Setup the tablet
1:01:51 Putting variables into RISC-V registers
1:04:03 Compare our answer to Chapter 2.1 Example 1 with the book
1:07:37 Chapter 2.1 continued, Memory Operands
1:09:51 Figure 2.2 - Memory addresses and contents of memory at those locations
1:15:26 Chapter 2.1 continued
1:16:05 Chapter 2.3 Example 2 - Compiling an Assignment When an Operand Is in Memory
1:16:51 Compiling g = h + A[8];
1:24:56 Compare our answer to Chapter 2.1 Example 2 with the book
1:25:32 Update our answer to use x22 as the base address
1:26:19 Continue to compare our answer to Chapter 2.1 Example 2
1:29:42 Leave the rest of Chapter 2.3 for the next day

Annotated by Miblo - https://handmade.network/m/Miblo
