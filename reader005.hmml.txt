0:02 Welcome to the stream
0:54 Recap the XOR tricks from last time and set the stage for the day
3:39 Chapter 2.6 - RV32I Conditional Branch: http://www.riscvbook.com/
7:50 Definition: bltu (for signed array bounds-checking)
9:46 Chapter 2.6 continued, What's Different in conditional branching between RISC-V and other architectures
10:53 Chapter 2.6 Elaboration 1 - Multiword addition without condition codes
13:37 Hunt for multibyte addition example in Code: The Hidden Language of Computer Hardware and Software: http://www.charlespetzold.com/code/
17:22 Chapter 19 - Two Classic Microprocessors
25:48 Chapter 19 continued, on the ADC (addition with carry) and SBB (subtraction with borrow) instructions in the Intel 8080 microprocessor
36:50 Imaginary RISC-V multibyte addition with carry
39:03 Chapter 19 continued, on the ADD, ADC pattern
39:32 Chapter 19 continued, on the 8080 flags: Carry, Zero, Sign, Parity and Auxiliary Carry
41:41 Thoughts on the differing complexity and terseness of architectures with (8080) and without (RISC-V) status flags
45:26 Chapter 2.6 Elaboration 1 - Multiword addition without condition codes
55:12 Wrap this up
56:05 Chat comment: "Thanks"

Annotated by Miblo - https://handmade.network/m/Miblo
